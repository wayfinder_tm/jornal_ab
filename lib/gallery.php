<?php if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly ?>
<?php

function random_string($length = 6) {
    $characters = "abcdefghijklmnopqrstuvwxyz";
    $string = '';    
    for ($p = 0; $p < $length; $p++) {
        $string .= $characters[mt_rand(0, strlen($characters))];
    }
    return $string;
}


/* 
 * Изменение вывода галереи через шоткод 
 * Смотреть функцию gallery_shortcode в http://wp-kama.ru/filecode/wp-includes/media.php
 * $output = apply_filters( 'post_gallery', '', $attr );
 */
add_filter('post_gallery', 'my_gallery_output', 10, 2);
function my_gallery_output( $output, $attr ){
	$ids_arr = explode(',', $attr['ids']);
	$ids_arr = array_map('trim', $ids_arr );

	$pictures = get_posts( array(
		'posts_per_page' => -1,
		'post__in'       => $ids_arr,
		'post_type'      => 'attachment',
		'orderby'        => 'post__in',
	) );

	if( ! $pictures ) return;

	$id = random_string(10);

	// Вывод
	$out = '
		<div class="main__gallery" id="'.$id.'">
			<div class="main__gallery-images">
				<div class="swiper-container">
					<div class="swiper-wrapper">
	';
	foreach( $pictures as $pic ){
		$src = $pic->guid;
		$t = esc_attr( $pic->post_title );
		$title = ( $t && false === strpos($src, $t)  ) ? $t : '';

		$caption = ( $pic->post_excerpt != '' ? $pic->post_excerpt : $title );
		$out .= '
			<div class="swiper-slide" data-caption="'.$caption.'">
				<picture>
					<div class="overlay" style="background-image: url('. esc_url($src) .')"></div>
					<img src="'. esc_url($src) .'" class="img-responsive" />
				</picture>
			</div>
		';
	}

	$out .= '
					</div>
					<div class="swiper-pagination"></div>
					<div class="main__gallery-prev"></div>
					<div class="main__gallery-next"></div>
				</div>
			</div>
			<div class="main__gallery-caption"></div>
		</div>
		<script>
			$(document).ready(function () {
				var gallerySwiper'.$id.' = new Swiper (\'#'.$id.' .swiper-container\', {
					// loop: true,
					pagination: {
						el: ".swiper-pagination",
						type: "fraction",
						renderFraction: function (currentClass, totalClass) {
						    return "<span class=\'" + currentClass + "\'></span>" +
						           "/" +
						           "<span class=\'" + totalClass + "\'></span>";
						}
					},
					navigation: {
						nextEl: ".main__gallery-next",
						prevEl: ".main__gallery-prev",
					},
					on: {
						init: function() {
							$("#'.$id.' .main__gallery-caption").html($("#'.$id.' .swiper-slide").eq(this.activeIndex).data("caption"));
						},
						slideChange: function() {
							$("#'.$id.' .main__gallery-caption").html($("#'.$id.' .swiper-slide").eq(this.activeIndex).data("caption"));
						},
					}
				})
			});
		</script>
	';

	return $out;
}