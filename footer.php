<?php if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly ?>
	
	<?php echo get_ads('J8'); ?>

	<footer class="footer">
		<div class="container">
			<div class="row align-items-stretch">
				<div class="col-12 col-lg-8 d-flex flex-column">
					<ul class="footer__menu">
						<?php 
							wp_nav_menu(array(
								'theme_location'	=> 'footer_menu',
								'menu_id'			=> 'footer_menu',
								'container'			=> false,
								'items_wrap'		=> '%3$s'
							));
						?>
					</ul>
				</div>
				<div class="col-12 col-lg-4 d-block d-sm-flex justify-content-between flex-lg-column align-items-lg-end justify-content-lg-start">
					<address class="footer__address">
						<strong>ООО «Автоброкер»</strong><br>
						ул. Гастелло, 42Б<br>
						Тел. <a href="tel: 8 800 550 88 20">8 800 550 88 20</a><br>
						Email: <a href="mailto: a-b.samara@yandex.ru">a-b.samara@yandex.ru</a>
						<br>
						<br>
						<a href="https://zen.yandex.ru/id/5be972c308fa0700ad1e9096" target="_blank">Мы в Яндекс Дзен</a>
					</address>
					<!-- <div class="footer__taxi">
						<script src="//yastatic.net/taxi-widget/ya-taxi-widget.js"></script><div class="ya-taxi-widget" data-use-location="true" data-size="s" data-theme="dark" data-title="Вызвать такси:" data-point-a="" data-point-b="50.207892,53.206558" data-ref="https%3A%2F%2Fdev.a-b63.ru" data-proxy-url="https://3.redirect.appmetrica.yandex.com/route?start-lat={start-lat}&amp;start-lon={start-lon}&amp;end-lat={end-lat}&amp;end-lon={end-lon}&amp;ref={ref}&amp;appmetrica_tracking_id=1178268795219780156" data-description="улица Гастелло, 42Б"></div>
					</div> -->
				</div>
			</div>
		</div>
		<div class="footer__copyright">
			<div class="container">
				<div class="row">
					<div class="col-12 col-sm-10">
						<p>© 2000-<?php echo date('Y'); ?> "<a href="//a-b63.ru/">Автоброкер</a>" - продажа подержанных автомобилей в Самаре.<br>Все права защищены. Не является публичной офертой.</p>
					</div>
					<div class="col-12 col-sm-2">
						<div class="footer__copyright-rating">
							18+
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer>
	
	<?php wp_footer(); ?>
	<script>
		$(document).on('click.modal', 'a[rel~="modalopen"]', function(event) {
			event.preventDefault();
			$(this).modal();
		});
	</script>


	<?php get_template_part('counters'); ?>

	<script id="bx24_form_link" data-skip-moving="true">
		(function(w,d,u,b){w['Bitrix24FormObject']=b;w[b] = w[b] || function(){arguments[0].ref=u;
				(w[b].forms=w[b].forms||[]).push(arguments[0])};
				if(w[b]['forms']) return;
				var s=d.createElement('script');s.async=1;s.src=u+'?'+(1*new Date());
				var h=d.getElementsByTagName('script')[0];h.parentNode.insertBefore(s,h);
		})(window,document,'https://bitrix.a-b63.ru/bitrix/js/crm/form_loader.js','b24form');

		b24form({"id":"29","lang":"ru","sec":"kuu01i","type":"link","click":""});
		b24form({"id":"28","lang":"ru","sec":"3nua85","type":"link","click":""});
		b24form({"id":"27","lang":"ru","sec":"gv0jc7","type":"link","click":""});
		b24form({"id":"26","lang":"ru","sec":"ttgvpz","type":"link","click":""});
		b24form({"id":"25","lang":"ru","sec":"e3e7kr","type":"link","click":""});
		b24form({"id":"24","lang":"ru","sec":"brka8a","type":"link","click":""});
		b24form({"id":"23","lang":"ru","sec":"stn3hj","type":"link","click":""});
		b24form({"id":"22","lang":"ru","sec":"1wem11","type":"link","click":""});
		b24form({"id":"21","lang":"ru","sec":"ij4dwd","type":"link","click":""});
		b24form({"id":"20","lang":"ru","sec":"3uybk3","type":"link","click":""});
	</script>

	<!-- Yandex.Metrika counter -->
	<script type="text/javascript" >
	   (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
	   m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
	   (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

	   ym(55563406, "init", {
	        clickmap:true,
	        trackLinks:true,
	        accurateTrackBounce:true,
	        webvisor:true
	   });
	</script>
	<noscript><div><img src="https://mc.yandex.ru/watch/55563406" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
	<!-- /Yandex.Metrika counter -->

</body>
</html>