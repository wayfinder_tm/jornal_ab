<?php if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly ?>
<?php

global $abdb;
$abdb = new wpdb('db_readonly', '3X4b9J5z', 'db', '192.168.0.2:3307');

get_template_part('lib/breadcrumbs');
get_template_part('lib/ajax');
get_template_part('lib/ads');
get_template_part('lib/cf7');
get_template_part('lib/gallery');
get_template_part('widgets/unregister_defaults');
get_template_part('widgets/widget_last_news');
get_template_part('widgets/widget_popular_videos');
get_template_part('widgets/widget_most_interesting');
get_template_part('widgets/widget_ads');

// Поддержка меню
add_theme_support('menus');
register_nav_menu( 'mobile_menu', 'Мобильное меню' );
register_nav_menu( 'header_menu_top', 'Меню в заголовке верхнее' );
register_nav_menu( 'header_menu_bottom', 'Меню в заголовке нижнее' );
register_nav_menu( 'footer_menu', 'Меню в подвале' );

// Поддержка форматов записей
add_theme_support( 'post-formats', array('video') );

// Поддержка миниатюр
if ( function_exists( 'add_theme_support' ) ) {
	add_theme_support( 'post-thumbnails' );
	add_image_size( 'news_thumb', 960, 540, true );
}

// Svg support
function cc_mime_types( $mimes ){
	$mimes['svg'] = 'image/svg+xml';
	return $mimes;
}
add_filter( 'upload_mimes', 'cc_mime_types' );

// Menu active class
add_filter('nav_menu_css_class' , 'special_nav_class' , 10 , 2);
function special_nav_class ($classes, $item) {
	if (in_array('current-menu-item', $classes) ){
		$classes[] = 'active ';
	}
	return $classes;
}

// Очистка тегов хэдера
remove_action( 'wp_head', 'wp_generator' );
remove_action( 'wp_head', 'wp_shortlink_wp_head', 10, 0 );
remove_action( 'wp_head', 'rel_canonical' );
remove_action( 'wp_head', 'feed_links_extra', 3 );
remove_action( 'wp_head', 'feed_links', 2 );
remove_action( 'wp_head', 'rsd_link' );
remove_action( 'wp_head', 'wlwmanifest_link' );
remove_action( 'wp_head', 'index_rel_link' );
remove_action( 'wp_head', 'start_post_rel_link', 10, 0 );
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );
remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 );


add_filter( 'login_headerurl', 'custom_login_header_url' );
function custom_login_header_url($url) {
	return '/';
}

add_filter( 'login_headertitle', 'custom_login_headertitle' );
function custom_login_headertitle($title) {
	return 'Сделано с любовью';
}

function remove_admin_bar_links() {
	global $wp_admin_bar;
	$wp_admin_bar->remove_menu('wp-logo');
	$wp_admin_bar->remove_menu('updates');
}
add_action( 'wp_before_admin_bar_render', 'remove_admin_bar_links' );

// Кол-во слов цитаты
function new_excerpt_length() { return '50'; }
add_filter('excerpt_length', 'new_excerpt_length');

// Окончание цитаты
function new_excerpt_more($more) {
	return '...';
}
add_filter('excerpt_more', 'new_excerpt_more');

// Выключаем xmlrpc, ибо дыра
add_filter('xmlrpc_enabled', '__return_false');

// Remove p tag from shortcodes
function shortcode_empty_paragraph_fix($content){   
    $array = array (
        '<p>[' => '[', 
        ']</p>' => ']', 
        ']<br />' => ']'
    );

    $content = strtr($content, $array);
    return $content;
}
add_filter('the_content', 'shortcode_empty_paragraph_fix');

// удаляет H2 из шаблона пагинации
add_filter('navigation_markup_template', 'my_navigation_template', 10, 2 );
function my_navigation_template( $template, $class ){
	return '
		<nav class="navigation %1$s" role="navigation">
			<div class="nav-links">%3$s</div>
		</nav>
	';
}

// Images class
function image_tag_class($class) {
    $class .= ' img-responsive';
    return $class;
}
add_filter('get_image_tag_class', 'image_tag_class' );

// Remove thumbnail height and width
add_filter( 'post_thumbnail_html', 'remove_thumbnail_dimensions', 10, 3 );
function remove_thumbnail_dimensions( $html, $post_id, $post_image_id ) {
    $html = preg_replace( '/(width|height)=\"\d*\"\s/', "", $html );
    return $html;
}

// Remove height and width from images in content
add_filter( 'the_content', 'remove_wps_width_attribute', 10 );
add_filter( 'post_thumbnail_html', 'remove_wps_width_attribute', 10 );
add_filter( 'image_send_to_editor', 'remove_wps_width_attribute', 10 );
function remove_wps_width_attribute( $html ) {
    $html = preg_replace( '/(width|height)=\"\d*\"\s/', "", $html );
    return $html;
}

// Wrap tables
function tables_wrapper($content) {
	return preg_replace_callback('~<table.*?</table>~is', function($match) {
		return '<div class="main__content-table">' . $match[0] . '</div>';
	}, $content);
}
add_filter('the_content', 'tables_wrapper');

// Sidebar
if ( function_exists('register_sidebar') )
	register_sidebar(array(
		'name' => 'Боковая панель',
		'id' => 'sidebar',
		'before_widget' => '
			<div id="%1$s" class="widget gray white__bg %2$s">
		',
		'after_widget' => '
			</div>
		',
		'before_title' => '
				<div class="widget__head">
		',
		'after_title' => '
				</div>
		',
	));

// Post views
function getPostViews($postID){
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
        return 0;
    }
    return $count;
}
function setPostViews($postID) {
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    }else{
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}
add_filter('manage_posts_columns', 'posts_column_views');
add_action('manage_posts_custom_column', 'posts_custom_column_views',5,2);
function posts_column_views($defaults){
    $defaults['post_views'] = __('Просмотров');
    return $defaults;
}
function posts_custom_column_views($column_name, $id){
    if($column_name === 'post_views'){
        echo getPostViews(get_the_ID());
    }
}


function modify_read_more_link() {
    return '';
}
add_filter( 'the_content_more_link', 'modify_read_more_link' );

add_filter( 'post_thumbnail_html', 'wpdd_modify_post_thumbnail_html', 10, 5 );
/**
 * Add `loading="lazy"` attribute to images output by the_post_thumbnail().
 *
 * @param  string $html	The post thumbnail HTML.
 * @param  int $post_id	The post ID.
 * @param  string $post_thumbnail_id	The post thumbnail ID.
 * @param  string|array $size	The post thumbnail size. Image size or array of width and height values (in that order). Default 'post-thumbnail'.
 * @param  string $attr	Query string of attributes.
 * 
 * @return string	The modified post thumbnail HTML.
 */
function wpdd_modify_post_thumbnail_html( $html, $post_id, $post_thumbnail_id, $size, $attr ) {

	return str_replace( '<img', '<img loading="lazy"', $html );

}