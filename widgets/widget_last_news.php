<?php if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly ?>
<?php
class lastNewsWidget extends WP_Widget {
 
	/*
	 * создание виджета
	 */
	function __construct() {
		parent::__construct(
			'last_news', 
			'Последние новости',
			array( 'description' => 'Выводит последние новости' )
		);
	}
 
	/*
	 * фронтэнд виджета
	 */
	public function widget( $args, $instance ) {
		
		if (!is_single()) return;

		$title = apply_filters( 'widget_title', $instance['title'] );
		$posts_per_page = $instance['posts_per_page'];
 
		echo $args['before_widget'];
 
		if (!empty($title))
			echo $args['before_title'] . $title . $args['after_title'];
 
		$q = new WP_Query(array(
			'posts_per_page' => $posts_per_page,
		    'tax_query' => array(
		        array(
		            'taxonomy' => 'post_format',
		            'field' => 'slug',
		            'terms' => array(
		                'post-format-video'
		            ),
		            'operator' => 'NOT IN'
		        )
		    )
		));
		if ($q->have_posts()) :
			echo "<ul>";
			while( $q->have_posts() ): $q->the_post();
				echo '<li><a href="' . get_the_permalink() . '">' . get_the_title() . '</a></li>';
			endwhile;
			echo "</ul>";
		endif;
		wp_reset_postdata();
 
		echo $args['after_widget'];
	}
 
	/*
	 * бэкэнд виджета
	 */
	public function form( $instance ) {
		if ( isset( $instance[ 'title' ] ) ) {
			$title = $instance[ 'title' ];
		}
		if ( isset( $instance[ 'posts_per_page' ] ) ) {
			$posts_per_page = $instance[ 'posts_per_page' ];
		}
		echo '
		<p>
			<label for="' . $this->get_field_id( 'title' ) . '">Заголовок</label> 
			<input class="widefat" id="' . $this->get_field_id( 'title' ) . '" name="' . $this->get_field_name( 'title' ) . '" type="text" value="' . esc_attr( $title ) . '" />
		</p>
		<p>
			<label for="' . $this->get_field_id( 'posts_per_page' ) . '">Количество постов:</label> 
			<input id="' . $this->get_field_id( 'posts_per_page' ) . '" name="' . $this->get_field_name( 'posts_per_page' ) . '" type="text" value="' . (($posts_per_page) ? esc_attr( $posts_per_page ) : '5') . '" size="3" />
		</p>
		';
	}
 
	/*
	 * сохранение настроек виджета
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		$instance['posts_per_page'] = ( is_numeric( $new_instance['posts_per_page'] ) ) ? $new_instance['posts_per_page'] : '10';
		return $instance;
	}
}
 
/*
 * регистрация виджета
 */
function last_news_widget_load() {
	register_widget( 'lastNewsWidget' );
}
add_action( 'widgets_init', 'last_news_widget_load' );