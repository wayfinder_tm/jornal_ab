<?php if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly ?>
<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<meta name="format-detection" cotent="telephone=no" />
	<title><?php echo wp_get_document_title(); ?></title>
	<meta name="yandex-verification" content="38db128b2fe53d0f" />

	<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
	<link rel="manifest" href="/site.webmanifest">
	<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#4f4f4f">
	<meta name="msapplication-TileColor" content="#4F4F4F">
	<meta name="theme-color" content="#4F4F4F">
	
	<?php wp_head(); ?>

	<!-- Fonts  -->
	<link rel="stylesheet" href="//st1.a-b63.ru/mag/fonts/icons/style.css">
	<link rel="stylesheet" href="//cdn.materialdesignicons.com/3.6.95/css/materialdesignicons.min.css">
	<link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&display=swap&subset=cyrillic,cyrillic-ext" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Montserrat:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&display=swap&subset=cyrillic,cyrillic-ext" rel="stylesheet">
	
	<link rel="stylesheet" href="//st1.a-b63.ru/mag/css/vendor.css">
	<link rel="stylesheet" href="//st1.a-b63.ru/mag/css/style.css">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/style.css">
	
	<script src="https://cdn.jsdelivr.net/npm/vue@2.6.10/dist/vue.min.js"></script>
	<script type="text/javascript" src="//st1.a-b63.ru/mag/js/vendor.js"></script>
	<script type="text/javascript" src="//st1.a-b63.ru/mag/js/forms.js"></script>
	<script type="text/javascript" src="//st1.a-b63.ru/mag/js/scripts.js"></script>

</head>
<body <?php body_class(); ?>>

	<?php echo get_ads('J1'); ?>

	<header class="header">	
	
		<div class="mmenu d-block d-lg-none">
			<div class="mmenu__bg">
				<div class="container">
					<div class="mmenu__bg-circle"></div>
				</div>
			</div>
			<div class="container">
				<div class="mmenu__menu">
					<ul>
						<?php 
							wp_nav_menu(array(
								'theme_location'	=> 'mobile_menu',
								'menu_id'			=> 'mobile_menu',
								'container'			=> false,
								'items_wrap'		=> '%3$s'
							));
						?>
					</ul>
				</div>
			</div>
		</div>
	
		<div class="header__mobile d-block d-lg-none">
			<div class="container">
				<div class="d-flex align-items-center justify-content-between">
	
					<button class="hamburger hamburger--slider" type="button">
						<span class="hamburger-box">
							<span class="hamburger-inner"></span>
						</span>
					</button>
	
					<a href="https://a-b63.ru" class="header__mobile-logo d-block d-md-none">
						<img src="//st1.a-b63.ru/mag/images/logo_light.svg" alt="A-B63" class="img-responsive">
					</a>
	
					<?php if ( is_user_logged_in() ) : ?>
					<a href="/wp-admin/" class="header__mobile-user">
						<span class="icon icon-user"></span>
					</a>
					<?php else : ?>
					<a href="/login/" class="header__mobile-user">
						<span class="icon icon-user"></span>
					</a>
					<?php endif; ?>
	
				</div>
			</div>
		</div>
	
		<div class="header__desktop d-none d-md-block">
			<div class="header__top d-none d-lg-block">
				<div class="container">
					<div class="d-flex align-items-center justify-content-between">
						<div class="header__top-left">
							<ul>
								<!-- <li>
									<a href="https://a-b63.ru" class="header__top-logo">
										<img src="//st1.a-b63.ru/mag/images/logo_light.svg" alt="A-B63" class="img-responsive">
									</a>
								</li> -->
								<li>
									<a href="https://a-b63.ru">Главная</a>
								</li>
								<?php 
									wp_nav_menu(array(
										'theme_location'	=> 'header_menu_top',
										'menu_id'			=> 'header_menu_top',
										'container'			=> false,
										'items_wrap'		=> '%3$s'
									));
								?>
							</ul>
						</div>
						<div class="header__top-right">
							<ul>
								<!-- <li class="header__city">
									<a href="#citys-modal" rel="modal:open">
										<i class="icon icon-point"></i> <span>Самара</span>
									</a>
								</li>
								<li class="header__favorite">
									<a href="#">
										<i class="icon icon-heart"></i> Избранное <div>11</div>
									</a>
								</li> -->
								<?php if ( is_user_logged_in() ) : ?>
								<?php $current_user = wp_get_current_user(); ?>
								<li class="header__account">
									<a href="/wp-admin/"><i class="icon icon-user"></i> <?php echo $current_user->display_name; ?></a>
								</li>
								<?php else : ?>
								<li class="header__signin">
									<i class="icon icon-user"></i>
									<a href="/login/">Вход</a>
									<?php if (get_option( 'users_can_register' ) == '1') : ?>
									<div></div>
									<a href="<?php echo site_url('/wp-login.php?action=register'); ?>">Регистрация</a>
									<?php endif; ?>
								</li>
								<?php endif; ?>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="header__center">
				<div class="container">
					<div class="row align-items-center">
						<div class="col-3 col-lg-4">
							<a href="/" class="header__center-logo">
								<img src="//st1.a-b63.ru/mag/images/logo.svg" alt="A-B63" class="img-responsive">
							</a>
						</div>
						<div class="col-4 d-none d-lg-block">
							<?php
							global $abdb;
								$cnt = $abdb->get_var('SELECT count(*) FROM `autos`  WHERE 1 AND `publish` = 1 AND `mark` != "0" AND `model` != "0" AND `selltype` != 8');
							?>
							<div class="header__center-counter">
								<a href="//a-b63.ru/"><?php echo $cnt ?></a>
								<span>автомобилей<br>в продаже</span>
							</div>
						</div>
						<div class="col-9 col-lg-4">
							<div class="header__center-phone">
								<a href="tel: 8 800 550 8820">8 800 550 8820</a>
								<p>Бесплатный звонок по России</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="header__bottom">
				<div class="container">
					<div class="d-flex justify-content-between">
						<nav id="nav">
							<ul>
								<?php 
									wp_nav_menu(array(
										'theme_location'	=> 'header_menu_bottom',
										'menu_id'			=> 'header_menu_bottom',
										'container'			=> false,
										'items_wrap'		=> '%3$s'
									));
								?>
							</ul>
						</nav>
						<div class="header__bottom-buttons d-none d-lg-flex">
							<div></div>
							<ul>
								<li>
									<a href="https://lk.a-b63.ru/" class="btn rounded yellow" style="margin: 0; font-size: 1em;" target="_blank">Разместить авто на сайт</a>
									<!-- <a href="#" class="btn rounded yellow" style="margin: 0;"><i class="icon icon-plus"></i> Продать авто</a> -->
									<!-- <ul>
										<li id="menu-item-85" class="diplicate menu-item menu-item-type-post_type menu-item-object-page menu-item-85"><a href="https://a-b63.ru/uslugi/buyout/">Оценить авто</a></li>
										<li id="menu-item-80" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-80"><a href="https://a-b63.ru/uslugi/buyout/">Срочная продажа</a></li>
										<li id="menu-item-82" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-82"><a href="https://a-b63.ru/uslugi/avcommission/">Продажа с гарантией и обеспечительным платежом</a></li>
										<li id="menu-item-81" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-81"><a href="https://a-b63.ru/uslugi/comission/">Комиссионная продажа</a></li>
										<li id="menu-item-83" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-83"><a href="https://a-b63.ru/uslugi/trade-in/">Обмен (Trade-in)</a></li>
										<li id="menu-item-84" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-84"><a href="https://a-b63.ru/uslugi/onlinecommission/">Онлайн комиссия</a></li>
									</ul> -->
								</li>
							</ul>
							<div></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	
		<!-- <div class="header__buttons d-block d-lg-none">
			<div class="container">
				<div class="d-flex flex-wrap justify-content-between">
					<a href="https://a-b63.ru/uslugi/buyout/" class="btn rounded yellow" target="_blank"><i class="icon icon-plus"></i> Продать авто</a>
					<a href="https://a-b63.ru/uslugi/buyout/" class="btn rounded white" target="_blank"><i class="icon icon-rate"></i> Оценить авто</a>
				</div>
			</div>
		</div> -->
	</header>

	<div id="jheader">
		<div class="container">
			<div class="jheader__top">
				<div class="d-block d-sm-flex align-items-center justify-content-between">
					<a href="/" class="jheader__title">Журнал</a>
					<form action="/" class="jheader__search d-none d-sm-flex">
						<button>
							<span class="icon icon-search"></span>
						</button>
						<input type="text" name="s" placeholder="Поиск">
					</form>
				</div>
			</div>
			<div class="jheader__bottom">
				<div class="jheader__bottom-menu">
					<div class="jheader__bottom-menu-inner">
						<ul>
							<li <?php if (is_home()) : ?>class="active"<?php endif; ?>>
								<a href="/">Все</a>
							</li>
							<?php
								wp_list_categories(array(
									'hide_empty' => 0,
									'depth' => 1,
									'title_li' => false,
								));
							?>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>