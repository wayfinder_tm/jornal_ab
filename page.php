<?php setPostViews(get_the_ID()); get_header(); ?>

	<div class="container">
		<div class="d-block d-lg-flex">
			<main class="main jornal">

				<?php ads_in_content_head(); ?>

				<?php
					if ( function_exists('kama_breadcrumbs') ) {
				  		kama_breadcrumbs('<span class="icon icon-arrow-right"></span>', array(
				  				'tag' => 'Публикации по тегу: %s',
				  				'tax_tag' => '%1$s из "%2$s" по тегу: %3$s',
								'search' => 'Результаты поиска по запросу - %s',
								'author' => 'Архив автора: %s',
								'year' => 'Архив за %d год',
								'month' => 'Архив за: %s',
				  			), array(
				  			'on_front_page' => false,
				  			'markup' => array(
				  				'wrappatt' => '<div id="breadcrumbs"><span class="icon icon-home"></span>%s</div>',
				  				'linkpatt' => '<a href="%s">%s</a>',
				  				'sep_after'=>'',
				  			)
				  		));
					}
				?>

				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				<div class="main__article">

					<div class="main__content">
						<h1><?php the_title(); ?></h1>
						<?php the_content(); ?>
					</div>

				</div>
				<?php endwhile; else : ?>
				<h2>Нет постов удовлетворяющих критериям поиска.</h2>
				<?php endif; ?>

				<?php ads_in_article(); ?>

			</main>

			<?php get_sidebar(); ?>

		</div>
	</div>

<?php get_footer(); ?>