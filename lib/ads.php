<?php if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly ?>
<?php

function get_ads($id = false)
{
	if (empty($id)) return false;

	global $wpdb;

	$result = $wpdb->get_row($wpdb->prepare("SELECT * FROM `{$wpdb->prefix}ads` WHERE `name` = %s LIMIT 1", $id), ARRAY_A);

	$ad = '<!-- ADS place ' . $id . ' //-->';

	if (!empty($result)) :
		$ads = $result['main'];

		if (empty($ads)) return;
		
		if (unserialize($ads) == false)
		{
			$ad .= $ads;
		}
		else
		{
			$ads = unserialize($ads);
			$ad .= $ads[array_rand($ads)];
		}
	endif;
	
	$ad .= '<!-- ADS place ' . $id . ' END //-->';

	return $ad;
}