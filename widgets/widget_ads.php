<?php if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly ?>
<?php
class adsWidget extends WP_Widget {
 
	/*
	 * создание виджета
	 */
	function __construct() {
		parent::__construct(
			'ads', 
			'Реклама',
			array( 'description' => 'Выводит рекламу' )
		);
	}
 
	/*
	 * фронтэнд виджета
	 */
	public function widget( $args, $instance ) {

		$title = apply_filters( 'widget_title', $instance['title'] );
		$content = $instance['text'];
 
		echo $args['before_widget'];
 
		if (!empty($title))
			echo $args['before_title'] . $title . $args['after_title'];

		echo $content;
 
		echo $args['after_widget'];
	}
 
	/*
	 * бэкэнд виджета
	 */
	public function form( $instance ) {
		if ( isset( $instance[ 'title' ] ) ) {
			$title = $instance[ 'title' ];
		}
		if ( isset( $instance[ 'text' ] ) ) {
			$text = $instance[ 'text' ];
		}
		echo '
		<p>
			<label for="' . $this->get_field_id( 'title' ) . '">Заголовок</label> 
			<input class="widefat" id="' . $this->get_field_id( 'title' ) . '" name="' . $this->get_field_name( 'title' ) . '" type="text" value="' . esc_attr( $title ) . '" />
		</p>
		<p>
			<label for="' . $this->get_field_id( 'text' ) . '">Код:</label> 
			<textarea class="widefat" cols="20" rows="16" id="' . $this->get_field_id( 'text' ) . '" name="' . $this->get_field_name( 'text' ) . '">' . (($text) ? esc_attr( $text ) : '') . '</textarea>
		</p>
		';
	}
 
	/*
	 * сохранение настроек виджета
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		$instance['text'] = ( ! empty( $new_instance['text'] ) ) ? $new_instance['text'] : '';
		return $instance;
	}
}
 
/*
 * регистрация виджета
 */
function ads_widget_load() {
	register_widget( 'adsWidget' );
}
add_action( 'widgets_init', 'ads_widget_load' );