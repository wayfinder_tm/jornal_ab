<?php if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly ?>
<?php

// /*Contact form 7 remove span*/
// add_filter('wpcf7_form_elements', function($content) {
//     $content = preg_replace('/<(span).*?class="\s*(?:.*\s)?wpcf7-form-control-wrap(?:\s[^"]+)?\s*"[^\>]*>(.*)<\/\1>/i', '\2', $content);

//     $content = str_replace('<br />', '', $content);
        
//     return $content;
// });

add_filter( 'wpcf7_form_elements', 'imp_wpcf7_form_elements' );
function imp_wpcf7_form_elements( $content ) {

    $str_pos_mark = strpos( $content, 'name="your-mark"' );
    if ($str_pos_mark) $content = substr_replace( $content, ' data-mark="true" data-model="#review_model" data-url="//st1.a-b63.ru/mag/json/marks.json" ', $str_pos_mark, 0 );

    $str_pos_model = strpos( $content, 'name="your-model"' );
    if ($str_pos_model) $content = substr_replace( $content, ' id="review_model" data-url="//st1.a-b63.ru/mag/json/models.json" disabled ', $str_pos_model, 0 );

    return str_replace('<p></p>', '', $content);
}