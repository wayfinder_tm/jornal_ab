<?php get_header(); ?>

	<div class="container">
		<div class="d-block d-lg-flex">
			<main class="main jornal">

				<?php echo get_ads('J2'); ?>

				<?php
					if (is_category()) :
						$category = get_category( get_query_var( 'cat' ) );
						if (count(get_categories( array( 'parent' => $category->cat_ID )))) :
				?>
				<div class="main__cats">
					<ul>
						<?php
							wp_list_categories(array(
								'show_option_none' => false,
								'child_of' => $category->cat_ID,
								'hide_empty' => 0,
								'depth' => 1,
								'title_li' => false,
							));
						?>
					</ul>
				</div>
				<?php
						endif;
					endif;
				?>

				<?php
					if ( function_exists('kama_breadcrumbs') ) {
						kama_breadcrumbs('<span class="icon icon-arrow-right"></span>', array(
								'tag' => 'Публикации по тегу: %s',
								'tax_tag' => '%1$s из "%2$s" по тегу: %3$s',
								'search' => 'Результаты поиска по запросу - %s',
								'author' => 'Архив автора: %s',
								'year' => 'Архив за %d год',
								'month' => 'Архив за: %s',
							), array(
							'on_front_page' => false,
							'markup' => array(
								'wrappatt' => '<div id="breadcrumbs"><span class="icon icon-home"></span>%s</div>',
								'linkpatt' => '<a href="%s">%s</a>',
								'sep_after'=>'',
							)
						));
					}
				?>
				
				<?php if (!is_home()) : ?>
				<div class="d-block d-sm-flex align-items-start justify-content-between"> 
					<div class="main__cat">
						<?php 
							if ( is_category() || is_tax() ) {
								single_cat_title(); 
							} elseif (is_tag()) {
								echo '#'; single_tag_title();
							} elseif (is_search()) {
								echo 'Результаты поиска по запросу - <b>' . get_search_query() . '</b>';
							}
						?>
					</div>
					<?php if (is_category('reviews')) : ?>
					<a href="/addreview/" class="btn small yellow rounded">Оставить отзыв</a>
					<?php endif; ?>
				</div>
				<?php endif; ?>

				<div class="main__articles">
					
					<?php $i = 0; if ( have_posts() ) : while ( have_posts() ) : the_post(); $i++; ?>
					<?php
						switch ($i) {
							case 4:
								echo get_ads('J3');
								break;
							case 7:
								echo get_ads('J4');
								break;
							case 10:
								echo get_ads('J5');
								break;
						}
					?>
					<article class="article">
						<header class="article__title">
							<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
							<?php if (in_category('reviews')) : ?>
							<span><?php the_field('subtitle'); ?></span>
							<?php endif; ?>
						</header>
						<?php
							if (get_post_format() == 'video') :
								$content = str_replace(array('[embed]', '[/embed]'), '', get_the_content());
								$links = preg_match(
									'/(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))(\S*)/',
									$content,
									$matches
								);
							endif;
						?>
						<?php if ( get_post_format() != 'video' && has_post_thumbnail() ) : ?>
						<figure class="article__image">
							<a href="<?php the_permalink(); ?>">
								<?php the_post_thumbnail( 'news_thumb' );  ?>
							</a>
						</figure>
						<?php elseif ($matches) : ?>
						<div class="article__image">
							<?php echo wp_oembed_get($matches[0]); ?>
						</div>
						<?php endif; ?>
						<div class="article__content">
							<?php
								if (get_post_format() == 'video' && !$matches) {
									the_content();
								} else {
									the_excerpt('');
								}
							?>
						</div>
						<div class="article__footer">
							<div class="article__footer-left">
								<?php the_tags( '<ul><li>','</li><li>','</li></ul>'); ?>
								<div>
									<span class="author">
										<?php
											if (get_field('author')) : 
												the_field('author');
											else :
												the_author();
											endif;
										?>
									</span>
									<span><?php echo get_the_date(); ?></span>
									<!-- <i class="icon icon-comment"></i>
									<a href="<?php the_permalink(); echo '#comments'; ?>">Комментарии</a>
									<span>(0)</span> -->
								</div>
							</div>
							<div class="article__footer-right">
								<a href="<?php the_permalink(); ?>" class="btn yellow">Читать далее</a>
							</div>
						</div>
					</article>
					<?php endwhile; else : ?>
					<?php if (is_search()) : ?>
					<h2>Нет постов удовлетворяющих критериям поиска.</h2>
					<?php else : ?>
					<h2>Раздел в процессе разработки.</h2>
					<?php endif; ?>
					<?php endif; ?>

				</div>

				<div class="main__pagination">
					<?php
						$args = array(
							'show_all'     => false, // показаны все страницы участвующие в пагинации
							'end_size'     => 1,     // количество страниц на концах
							'mid_size'     => 1,     // количество страниц вокруг текущей
							'prev_next'    => true,  // выводить ли боковые ссылки "предыдущая/следующая страница".
							'prev_text'    => '<i class="icon icon-arrow-left"></i>',
							'next_text'    => '<i class="icon icon-arrow-right"></i>',
							'add_args'     => false, // Массив аргументов (переменных запроса), которые нужно добавить к ссылкам.
							'add_fragment' => '',     // Текст который добавиться ко всем ссылкам.
							'screen_reader_text' => '',
						);
						the_posts_pagination( $args );
					?>
				</div>

			</main>

			<?php get_sidebar(); ?>

		</div>
	</div>

<?php get_footer(); ?>