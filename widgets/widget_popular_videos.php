<?php if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly ?>
<?php
class popularVideoWidget extends WP_Widget {
 
	/*
	 * создание виджета
	 */
	function __construct() {
		parent::__construct(
			'popular_videos', 
			'Популярное видео',
			array( 'description' => 'Выводит популярное видео' )
		);
	}
 
	/*
	 * фронтэнд виджета
	 */
	public function widget( $args, $instance ) {
		$title = apply_filters( 'widget_title', $instance['title'] );
		$posts_per_page = $instance['posts_per_page'];
 
		echo $args['before_widget'];
 
		if (!empty($title))
			echo $args['before_title'] . $title . $args['after_title'];
 
		$q = new WP_Query(array(
			'posts_per_page' => 10,
		    'tax_query' => array(
		        array(
		            'taxonomy' => 'post_format',
		            'field' => 'slug',
		            'terms' => array(
		                'post-format-video'
		            ),
		        )
		    ),
		    'orderby' => 'meta_value',
		    'meta_key' => 'post_views_count'
		));
		if ($q->have_posts()) :
			echo '<div class="row">';
			while( $q->have_posts() ): $q->the_post();
				$content = str_replace(array('[embed]', '[/embed]'), '', get_the_content());
				$links = preg_match(
					'/(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))(\S*)/',
					$content,
					$matches
				);
				if (empty($links)) continue;
				$cnt++;
				echo '
				<a href="' . get_the_permalink() . '" class="article col-12 col-sm-6 col-lg-12">
					<figure class="article__image">
						<i class="mdi mdi-youtube"></i>
						<img src="https://img.youtube.com/vi/' . $matches[1] . '/maxresdefault.jpg" alt="">
					</figure>
					<header class="article__title">
						<span>' . get_the_title() . '</span>
					</header>
				</a>
				';
				if ($cnt == 2) break;
			endwhile;
			echo "</div>";
		endif;
		wp_reset_postdata();
 
		echo $args['after_widget'];
	}
 
	/*
	 * бэкэнд виджета
	 */
	public function form( $instance ) {
		if ( isset( $instance[ 'title' ] ) ) {
			$title = $instance[ 'title' ];
		}
		echo '
		<p>
			<label for="' . $this->get_field_id( 'title' ) . '">Заголовок</label> 
			<input class="widefat" id="' . $this->get_field_id( 'title' ) . '" name="' . $this->get_field_name( 'title' ) . '" type="text" value="' . esc_attr( $title ) . '" />
		</p>
		';
	}
 
	/*
	 * сохранение настроек виджета
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		return $instance;
	}
}
 
/*
 * регистрация виджета
 */
function popular_videos_widget_load() {
	register_widget( 'popularVideoWidget' );
}
add_action( 'widgets_init', 'popular_videos_widget_load' );