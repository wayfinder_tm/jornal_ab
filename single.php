<?php setPostViews(get_the_ID()); get_header(); ?>

	<div class="container">
		<div class="d-block d-lg-flex">

			<main class="main jornal">

				<?php echo get_ads('J2'); ?>

				<?php
					if ( function_exists('kama_breadcrumbs') ) {
				  		kama_breadcrumbs('<span class="icon icon-arrow-right"></span>', array(
				  				'tag' => 'Публикации по тегу: %s',
				  				'tax_tag' => '%1$s из "%2$s" по тегу: %3$s',
								'search' => 'Результаты поиска по запросу - %s',
								'author' => 'Архив автора: %s',
								'year' => 'Архив за %d год',
								'month' => 'Архив за: %s',
				  			), array(
				  			'on_front_page' => false,
				  			'markup' => array(
				  				'wrappatt' => '<div id="breadcrumbs"><span class="icon icon-home"></span>%s</div>',
				  				'linkpatt' => '<a href="%s">%s</a>',
				  				'sep_after'=>'',
				  			)
				  		));
					}
				?>

				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				<div class="main__article">

					<script src="https://yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
					<script src="https://yastatic.net/share2/share.js"></script>
					<div class="ya-share2" data-services="telegram,vkontakte,facebook,odnoklassniki,twitter,viber,whatsapp"></div>

					<div class="main__content">
						<h1>
							<?php the_title(); ?>
							<?php if (get_field('subtitle')) : ?>
							<span><?php the_field('subtitle'); ?></span>
							<?php endif; ?>
						</h1>
						<?php the_content(); ?>
					</div>
					
					<div class="ya-share2" data-services="telegram,vkontakte,facebook,odnoklassniki,twitter,viber,whatsapp"></div>

					<div class="main__article-footer">
						<?php the_tags( '<ul><li>','</li><li>','</li></ul>'); ?>
						<div>
							<?php if (get_field('author')) : ?>
							<span class="author"><?php the_field('author'); ?></span>
							<?php endif; ?>
							<span><?php echo get_the_date(); ?></span>
						</div>
					</div>

					<div class="main__comments">
						<?php comments_template(); ?> 
					</div>

				</div>
				<?php endwhile; else : ?>
				<h2>Нет постов удовлетворяющих критериям поиска.</h2>
				<?php endif; ?>

			</main>

			<?php get_sidebar(); ?>

		</div>
	</div>

<?php get_footer(); ?>