<?php if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly ?>
<?php
class mostInterestingWidget extends WP_Widget {
 
	/*
	 * создание виджета
	 */
	function __construct() {
		parent::__construct(
			'most_interesting', 
			'Самое интересное',
			array( 'description' => 'Выводит самые интересные новости' )
		);
	}
 
	/*
	 * фронтэнд виджета
	 */
	public function widget( $args, $instance ) {

		$title = apply_filters( 'widget_title', $instance['title'] );
		$posts_per_page = $instance['posts_per_page'];
 
		echo $args['before_widget'];
 
		if (!empty($title))
			echo $args['before_title'] . $title . $args['after_title'];
 
		$q = new WP_Query(array(
			'posts_per_page' => $posts_per_page,
		    'tax_query' => array(
		        array(
		            'taxonomy' => 'post_format',
		            'field' => 'slug',
		            'terms' => array(
		                'post-format-video'
		            ),
		            'operator' => 'NOT IN'
		        )
		    ),
		    'meta_query' => array(
		        array(
		            'key' => '_thumbnail_id'
		        ) 
		    ),
		    'orderby' => 'meta_value',
		    'meta_key' => 'post_views_count'
		));
		if ($q->have_posts()) :
			echo '<div class="row">';
			while( $q->have_posts() ): $q->the_post();
				$cnt++;
				echo '
					<a href="' . get_the_permalink() . '" class="article ' . ($cnt <= 2 ? 'col-12 col-sm-6' : 'd-none d-lg-block') . ' col-lg-12">
						<figure class="article__image">
							' . get_the_post_thumbnail( get_the_ID(), 'news_thumb' ) . '
						</figure>
						<header class="article__title">
							<span>' . get_the_title() . '</span>
						</header>
					</a>
				';
			endwhile;
			echo '</div>';
		endif;
		wp_reset_postdata();
 
		echo $args['after_widget'];
	}
 
	/*
	 * бэкэнд виджета
	 */
	public function form( $instance ) {
		if ( isset( $instance[ 'title' ] ) ) {
			$title = $instance[ 'title' ];
		}
		if ( isset( $instance[ 'posts_per_page' ] ) ) {
			$posts_per_page = $instance[ 'posts_per_page' ];
		}
		echo '
		<p>
			<label for="' . $this->get_field_id( 'title' ) . '">Заголовок</label> 
			<input class="widefat" id="' . $this->get_field_id( 'title' ) . '" name="' . $this->get_field_name( 'title' ) . '" type="text" value="' . esc_attr( $title ) . '" />
		</p>
		<p>
			<label for="' . $this->get_field_id( 'posts_per_page' ) . '">Количество постов:</label> 
			<input id="' . $this->get_field_id( 'posts_per_page' ) . '" name="' . $this->get_field_name( 'posts_per_page' ) . '" type="text" value="' . (($posts_per_page) ? esc_attr( $posts_per_page ) : '3') . '" size="3" />
		</p>
		';
	}
 
	/*
	 * сохранение настроек виджета
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		$instance['posts_per_page'] = ( is_numeric( $new_instance['posts_per_page'] ) ) ? $new_instance['posts_per_page'] : '10';
		return $instance;
	}
}
 
/*
 * регистрация виджета
 */
function most_interesting_widget_load() {
	register_widget( 'mostInterestingWidget' );
}
add_action( 'widgets_init', 'most_interesting_widget_load' );