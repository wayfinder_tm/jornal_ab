<?php if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly ?>
<?php

if (is_user_logged_in()) {
	add_action('init', 'ajax_init');
} else {
	add_action('init', 'ajax_nopriv_init');
}

function ajax_nopriv_init(){
	add_action( 'wp_ajax_nopriv_last', 'ajax_last' );
}

function ajax_init(){
	add_action( 'wp_ajax_last', 'ajax_last' );
}

function ajax_last() {
	$q = new WP_Query(array(
		'posts_per_page' => 4,
		'post_status' => 'publish',
		'tax_query' => array(
			array(
				'taxonomy' => 'post_format',
				'field' => 'slug',
				'terms' => array(
					'post-format-video'
				),
				'operator' => 'NOT IN'
			)
		),
		'meta_query' => array(
			array(
				'key' => '_thumbnail_id'
			) 
		),
	));
	if ($q->have_posts()) :
		while( $q->have_posts() ): $q->the_post();
			echo '
				<a href="' . get_the_permalink() . '" class="article" target="_blank">
					<figure class="article__image">
						<img src="data:image/svg+xml,%3Csvg xmlns=\'http://www.w3.org/2000/svg\' viewBox=\'0 0 1 1\'%3E%3C/svg%3E" data-src="'.get_the_post_thumbnail_url(get_the_ID(), 'thumbnail').'" class="lazy">
					</figure>
					<div class="article__title">
						' . get_the_title() . '
					</div>
				</a>
			';
		endwhile;
	endif;
	die();
}